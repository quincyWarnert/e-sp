package com.example.esp

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import com.example.esp.Utils.InjectorUtils
import com.example.esp.login.LoginViewModel

class LoginActivity : AppCompatActivity() {
    val viewModel: LoginViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        initializerUi()
    }
    fun initializerUi(){
        val factory= InjectorUtils.provideLoginViewModelFactory()
        val viewModel= ViewModelProviders.of(this).get(LoginViewModel::class.java)

    }


}
