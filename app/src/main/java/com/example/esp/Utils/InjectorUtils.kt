package com.example.esp.Utils

import com.example.esp.data.FakeDatabase
import com.example.esp.login.LoginRepository
import com.example.esp.login.LoginViewModel
import com.example.esp.register.RegisterRepository
import com.example.esp.register.RegisterVIewModelFactory
import com.example.esp.register.RegisterViewModel

object InjectorUtils {
    fun provideRegisterViewModelFactory():RegisterViewModel{
        val registerRepository=RegisterRepository.getInstance(FakeDatabase.getInstance().registerDao)
        return RegisterViewModel(registerRepository)
    }
    fun provideLoginViewModelFactory():LoginViewModel{
        val loginRepository= LoginRepository.getInstance(FakeDatabase.getInstance().loginDao)
        return LoginViewModel(loginRepository)
    }
}