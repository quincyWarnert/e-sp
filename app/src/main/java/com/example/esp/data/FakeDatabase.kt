package com.example.esp.data

import com.example.esp.login.LoginDao
import com.example.esp.register.RegisterDao

class FakeDatabase private constructor() {
    var registerDao= RegisterDao()
    var loginDao= LoginDao()
    private set

    companion object{
        @Volatile private var instance:FakeDatabase?=null

        fun getInstance()= instance?: synchronized(this){
            instance?:FakeDatabase().also{instance=it}
        }
    }
}