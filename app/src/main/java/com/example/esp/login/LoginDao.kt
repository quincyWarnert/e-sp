package com.example.esp.login

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class LoginDao {

    private val loggedInUserList=mutableListOf<Login>()
    private val loggedUser = MutableLiveData<List<Login>>()

    init {
        loggedUser.value=loggedInUserList
    }
    fun LoginUser(newUser:Login){
        loggedInUserList.add(newUser)

        loggedUser.value=loggedInUserList
    }

    fun getLoggedInUser()=loggedUser as LiveData<List<Login>>
}