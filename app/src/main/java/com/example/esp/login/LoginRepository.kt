package com.example.esp.login

class LoginRepository private constructor(private val loginDao: LoginDao) {
    fun getLoggedInUser()=loginDao.getLoggedInUser()
    fun loginUser(loginUser:Login)=loginDao.LoginUser(loginUser)


    companion object {
        @Volatile
        private var instance: LoginRepository? = null

        fun getInstance(loginDao: LoginDao) =
            instance ?: synchronized(this) {
                instance ?: LoginRepository(loginDao).also { instance = it }
            }
    }
}