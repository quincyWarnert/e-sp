package com.example.esp.login

import androidx.lifecycle.ViewModel

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {
    fun getLoggedInUser()=loginRepository.getLoggedInUser()
    fun logUserIn(user:Login)=loginRepository.loginUser(user)
}