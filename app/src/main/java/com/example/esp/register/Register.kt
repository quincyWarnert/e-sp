package com.example.esp.register

import android.util.Patterns
import androidx.databinding.BaseObservable


data class Register(
    private var register_name: String,
    private var register_email: String,
    private var register_password: String
):BaseObservable() {

    fun LoginUser(EmailAddress: String, Password: String) {
        register_email = EmailAddress
        register_password = Password
    }

    fun getRegisterEmail(): String {
        return register_email
    }

    fun getRegisterName(): String {
        return register_name
    }

    fun getRegisterPassword(): String {
        return register_password
    }

    fun isEmailValid(): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(getRegisterEmail()).matches()
    }


    fun isPasswordLengthGreaterThan5(): Boolean {
        return getRegisterPassword().length > 6
    }

}