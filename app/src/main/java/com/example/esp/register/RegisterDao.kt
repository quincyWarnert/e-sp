package com.example.esp.register

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData

class RegisterDao {

    private val registerNewUsersList=mutableListOf<Register>()
    private val newUsers =MutableLiveData<List<Register>>()

    init {
        newUsers.value=registerNewUsersList
    }
    fun addNewUser(newUser:Register){
        registerNewUsersList.add(newUser)
        newUsers.value=registerNewUsersList
    }

    fun getNewUsers()=newUsers as LiveData<List<Register>>
}