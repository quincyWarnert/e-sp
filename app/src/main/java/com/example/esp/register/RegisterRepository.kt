package com.example.esp.register

class RegisterRepository private constructor(private val registerDao: RegisterDao) {

    fun addUser(registerUser: Register) {
        registerDao.addNewUser(registerUser)
    }

    fun getNewuser() = registerDao.getNewUsers()

    companion object {
        @Volatile
        private var instance: RegisterRepository? = null

        fun getInstance(registerDao: RegisterDao) =
            instance ?: synchronized(this) {
                instance ?: RegisterRepository(registerDao).also { instance = it }
            }
    }
}