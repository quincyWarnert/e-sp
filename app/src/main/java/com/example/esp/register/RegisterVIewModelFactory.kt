package com.example.esp.register

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

class RegisterVIewModelFactory(private val registerRepository: RegisterRepository):ViewModelProvider.NewInstanceFactory() {

    @Suppress("UNCHECKED_CAST")
    override fun <T: ViewModel?>create(modelClass:Class<T>):T{
        return RegisterViewModel(registerRepository)as T
    }
}